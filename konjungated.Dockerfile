# Konjungate Dockerfile
#
# Author: Thesix
#
# Version 0.1.1

FROM ubuntu:bionic
LABEL maintainer="thesix, https://thesix.mur.at/"

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends software-properties-common && \
    DEBIAN_FRONTEND=noninteractive apt-add-repository -y ppa:bitcoin/bitcoin && \
    DEBIAN_FRONTEND=noninteractive apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    libboost-system1.65 \
    libboost-filesystem1.65 \
    libboost-program-options1.65 \
    libboost-thread1.65 \
    libboost-chrono1.65 \
    libdb5.3++ \
    libevent-2.1-6 \
    libevent-pthreads-2.1-6 \
    tzdata \
    libssl1.1 \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN ln -fs /usr/share/zoneinfo/Europe/Vienna /etc/localtime && dpkg-reconfigure -f noninteractive tzdata; mkdir /app
ADD Konjungate/src/Konjungated /app

EXPOSE 19417
# WORKDIR /app/konjungate
ENTRYPOINT ["/app/Konjungated"]
