# Readme

Container images for the [konjungate crypto
currency](https://www.konjungate.net/).  Images are built here on gitlab.com and
can be downloaded through gitlab.com's registry.  Simplest way to use the images
is to use the docker-compose.yml files.  Just use one of the two files and
rename them to `docker-compose.yml`.  Then start the container by running
`docker-compose up -d`.

## Konjungated cmdline for linux

Use the `konjungated.docker-compose.yml` file to run the daemon.  This is
suitable for masternodes.

## Konjungate-qt gui for linux

Use the `konjungate-qt.docker-compose.yml`fiel.  You might want to adjust the
UID/GID for the user so it runs under your UID.
