# Konjungate build Dockerfile
#
# Author: Thesix
#
# Version 0.0.1

FROM ubuntu:bionic
LABEL maintainer="thesix, https://thesix.mur.at/"

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get upgrade -y --no-install-recommends && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends software-properties-common && \
    DEBIAN_FRONTEND=noninteractive apt-add-repository -y ppa:bitcoin/bitcoin && \
    DEBIAN_FRONTEND=noninteractive apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" \
    make \
    build-essential \
    ntp \
    libtool \
    autoconf \
    automake \
    yasm \
    binutils \
    autogen \
    libboost-all-dev \
    libcurl4-openssl-dev \
    openssl \
    curl \
    libzip-dev \
    libminiupnpc-dev \
    libssl-dev \
    libsecp256k1-dev \
    bsdmainutils \
    libdb-dev \
    libdb++-dev \
    libminiupnpc-dev \
    libgmp-dev \
    libgmp3-dev \
    ufw \
    pkg-config \
    libevent-dev \
    unzip \
    libzmq5 \
    libcrypto++-dev \
    libqrencode-dev \
    qt5-default \
    qt5-qmake \
    libqt5gui5 \
    libqt5core5a \
    libqt5dbus5 \
    qttools5-dev \
    qttools5-dev-tools \
    libprotobuf-dev \
    protobuf-compiler \
    qtbase5-dev-tools \
    qttools5-dev-tools \
    tzdata \
    wget \
    tar \
    git \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN ln -snf /usr/share/zoneinfo/Europe/Vienna /etc/localtime && dpkg-reconfigure -f noninteractive tzdata

RUN wget http://download.oracle.com/berkeley-db/db-6.2.32.NC.tar.gz; tar zxf db-6.2.32.NC.tar.gz; cd db-6.2.32.NC/build_unix; ../dist/configure --enable-cxx --disable-shared; make; make install; ln -s /usr/local/BerkeleyDB.6.2/lib/libdb-6.2.so /usr/lib/libdb-6.2.so; ln -s /usr/local/BerkeleyDB.6.2/lib/libdb_cxx-6.2.so /usr/lib/libdb_cxx-6.2.so

# ADD build /app/
# VOLUME ["/out"]
# ENTRYPOINT ["/app/build"]
